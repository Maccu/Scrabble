﻿using System.Text;

namespace Scrabble
{
	public class ScrabbleTask2Service : ScrabbleService
	{
		private readonly IWordsDataService wordsData;

		public ScrabbleTask2Service(IWordsDataService wordsData)
		{
			this.wordsData = wordsData;
		}

		/// <summary>
		/// Finds list of possible words in a hand based on given length
		/// </summary>
		/// <param name="hand"></param>
		/// <param name="length"></param>
		/// <returns>List of acceptable/possible words</returns>
		private List<string> FindPossibleWordsInHand(string hand, int length = 2)
		{
			List<string> words = new();

			var handWords = SubStrings(hand, length).Distinct();

			foreach (var possibleWord in handWords)
			{
				if (wordsData.IsWord(possibleWord))
				{
					words.Add(possibleWord);
				}
			}

			return words;
		}

		/// <summary>
		/// Find all combinations of characters based on a given length
		/// </summary>
		/// <param name="value"></param>
		/// <param name="length"></param>
		/// <returns>Returns an IEnumerable of strings</returns>
		private static IEnumerable<string> SubStrings(string value, int length = 2)
		{
			if (string.IsNullOrEmpty(value))
			{
				yield break;
			}
			else if (length <= 0)
			{
				yield break;
			}

			for (int i = 0; i < value.Length; ++i)
			{
				StringBuilder sb = new (length);

				for (int j = 0; j < length; ++j)
				{
					sb.Append(value[(i + j) % value.Length]);
				}

				yield return sb.ToString();
			}
		}

		/// <summary>
		/// Returns a string of characters taken from
		/// the list of available tiles
		/// </summary>
		/// <returns>string of tiles</returns>
		private string DealTiles()
		{
			string hand = "";

			for (int i = 0; i < startingHandLength; i++)
			{
				var tile = GetTileFromAvailableTiles();

				if (tile != null)
				{
					hand += tile;
				}
			}

			return hand;
		}

		/// <summary>
		/// Attempts to take a single character from the
		/// string of available characters
		/// </summary>
		/// <returns></returns>
		private char? GetTileFromAvailableTiles()
		{
			if (AvailableTiles == "")
			{
				return null;
			}

			int position;
			lock (randLock)
			{
				position = random.Next(AvailableTiles.Length);
			}

			char result = AvailableTiles[position];
			AvailableTiles = AvailableTiles.Substring(0, position) + AvailableTiles.Substring(position + 1);
			return result;
		}

		/// <summary>
		/// Fills the AvailableTiles string with the appropriate number
		/// of characters
		/// </summary>
		public void SetupNewGame()
		{
			Console.WriteLine("TASK 2 | Starting a new game of Scrabble");

			AvailableTiles = string.Join("",
				new string('A', 9),
				new string('B', 2),
				new string('C', 2),
				new string('D', 4),
				new string('E', 12),
				new string('F', 2),
				new string('G', 3),
				new string('H', 2),
				new string('I', 9),
				new string('J', 1),
				new string('K', 1),
				new string('L', 4),
				new string('M', 2),
				new string('N', 6),
				new string('O', 8),
				new string('P', 2),
				new string('Q', 1),
				new string('R', 6),
				new string('S', 4),
				new string('T', 6),
				new string('U', 4),
				new string('V', 2),
				new string('W', 2),
				new string('X', 1),
				new string('Y', 2),
				new string('Z', 1),
				new string('*', 2)
			);

			var playerHands = new List<string>();

			for (int i = 0; i < numberOfPlayers; i++)
			{
				playerHands.Add(DealTiles());

				Console.WriteLine(playerHands[i]);
				var playerWords = FindPossibleWordsInHand(playerHands[i]);

				foreach (var word in playerWords)
				{
					Console.Write(word + " () ");
				}

				Console.WriteLine("\n");
			}
		}
	}
}
