﻿using Microsoft.Extensions.DependencyInjection;
using Scrabble;

var builder = new ServiceCollection()
	.AddSingleton<IWordsDataService, WordsDataService>()
	.AddSingleton<ScrabbleTask1Service>()
	.AddSingleton<ScrabbleTask2Service>()
	.BuildServiceProvider();

ScrabbleTask1Service task1 = builder.GetRequiredService<ScrabbleTask1Service>();
ScrabbleTask2Service task2 = builder.GetRequiredService<ScrabbleTask2Service>();

task1.SetupNewGame();
task2.SetupNewGame();
