﻿using System.Collections.ObjectModel;

namespace Scrabble
{
	public abstract class ScrabbleService
	{
		private readonly ReadOnlyDictionary<char, int> letters = new ReadOnlyDictionary<char, int>(
			new Dictionary<char, int>
			{
				['A'] = 1,
				['B'] = 3,
				['C'] = 3,
				['D'] = 2,
				['E'] = 1,
				['F'] = 4,
				['G'] = 2,
				['H'] = 4,
				['I'] = 1,
				['J'] = 8,
				['K'] = 5,
				['L'] = 1,
				['M'] = 3,
				['N'] = 1,
				['O'] = 1,
				['P'] = 3,
				['Q'] = 10,
				['R'] = 1,
				['S'] = 1,
				['T'] = 1,
				['U'] = 1,
				['V'] = 4,
				['W'] = 4,
				['X'] = 8,
				['Y'] = 4,
				['Z'] = 10,
				['*'] = 0
			});

		public string AvailableTiles = "";

		protected static readonly Random random = new();
		protected static readonly object randLock = new();
		protected readonly int startingHandLength = 7;
		protected readonly int numberOfPlayers = 2;
	}
}
