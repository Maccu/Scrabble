﻿
namespace Scrabble
{
	public class WordsDataService : IWordsDataService
	{
		private HashSet<string> words;
		private readonly string dataFile = "Data\\words.txt";

		public WordsDataService()
		{
			LoadWords();
		}

		public bool IsWord(string word)
		{
			if (words == null)
			{
				LoadWords();
			}

			return words.Contains(word.ToLower());
		}

		private void LoadWords()
		{
			words = new HashSet<string>();

			using StreamReader sr = new (dataFile);
			while (!sr.EndOfStream)
			{
				var word = sr.ReadLine();
				if (word != null)
				{
					words.Add(word);
				}
			}
		}
	}
}
