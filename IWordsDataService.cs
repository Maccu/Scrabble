﻿namespace Scrabble
{
	public interface IWordsDataService
	{
		public bool IsWord(string word);
	}
}
